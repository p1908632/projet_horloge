import pygame
import Color
class Led:
    nbLeds = 0
    nbLedsAllume = 0
    def __init__(self, colorOn, colorOff, coord, windows_surface):
        self.__colorOn = colorOn
        self.__colorOff = colorOff
        self.__color = colorOff
        self.__coord = coord
        self.__windows_surface = windows_surface
        self.__id = Led.nbLeds
        self.__isOn = False
        Led.nbLeds += 1

    def getId(self):
        return self.__id

    def getColor(self):
        return self.__color

    def getWindows_Surface(self):
        return self.__windows_surface

    def getCoord(self):
        return self.__coord

    def getIsOn(self):
        return self.__isOn

    def switchOn(self):
        if(self.getIsOn()):
            print("Led", self.getId(), "déja Allumé")
        else:
            self.__isOn = True
            self.__color = self.__colorOn
            pygame.draw.circle(self.getWindows_Surface(), self.getColor(),  self.getCoord(), 7)  # dessiner un cercle rempli, la surface sur laquelle nous voulons dessiner, la couleur,coordonné,rayon,remplie ou non
            self.nbLedsAllume += 1
    def switchOff(self):
        if not self.getIsOn():
            print("Led", self.getId(), "déja Eteint")
        else:
            self.__isOn = False
            self.__color = self.__colorOff
            pygame.draw.circle(self.getWindows_Surface(), self.getColor(),  self.getCoord(), 7)  # dessiner un cercle rempli, la surface sur laquelle nous voulons dessiner, la couleur,coordonné,rayon,remplie ou non
            self.nbLedsAllume -= 1

    def switchAlert(self):
        self.__isOn = False
        self.__color = Color().Yellow
        pygame.draw.circle(self.getWindows_Surface(), self.getColor(),  self.getCoord(), 7)  # dessiner un cercle rempli, la surface sur laquelle nous voulons dessiner, la couleur,coordonné,rayon,remplie ou non
