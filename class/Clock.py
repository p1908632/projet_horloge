import pygame


from Window import Window
from Led import Led
from Watch import Watch
from Text import Text
from Color import Color
import math
import time
import threading
import sys
class Clock:

    def __init__(self, colorWindow, colorLeds):
        self.__window = Window(colorWindow)
        self.__colorLeds = colorLeds
        self.__Leds = []
        self.__Leds_inverse = []
        self.__radius = int(0.9*self.__window.getHeigth()/2)
        self.__watch = None
        self.__text = Text(self)
        self.__boolTextInit = False
        self.__textPrec = None
        self.__delayPrintTime = 0
        self.__delayRAZ = 0
        self.__delayBlinking = 0
        self.__initDelayPrintTime = True
        self.__initDelayRect = True
        self.__initDelayRAZ = True
        self.__initDelayBlinking = True

    def getWindow(self):
        return self.__window

    def getColorLeds(self):
        return self.__colorLeds

    def getRadius(self):
        return self.__radius

    def getWatch(self):
        return self.__watch

    def getLeds(self):
        return self.__Leds

    def getLeds_inverse(self):
        return self.__Leds_inverse

    def getText(self):
        return self.__text

    def print_time(self):
        if not(self.__boolTextInit):
            self.__textPrec = self.getWatch().printTime(False)
            self.getText().displayTextCenter2(self.getRadius()//4, self.__textPrec, self.getColorLeds())
            self.__boolTextInit = True
        else:
            if self.__initDelayRect:
                start = time.time()
            self.getText().displayRectangle(self.getRadius(), self.getWindow().getColor(),)
            self.__textPrec = self.getWatch().printTime(False)
            self.getText().displayTextCenter2(self.getRadius()//4, self.__textPrec, self.getColorLeds())
            if self.__initDelayRect:
                self.__delayPrintTime += time.time() - start
                self.__initDelayRect = False

    def generate(self):
        self.getWindow().generate()
        self.__watch = Watch()
        i=0
        while i <= self.getWatch().getSecond().getMax():
            x = int(self.getWindow().getCenter()[0] + self.getRadius()*math.cos(math.pi/2 - i*2*math.pi/60))
            y = int(self.getWindow().getCenter()[1] - self.getRadius()*math.sin(math.pi/2 - i*2*math.pi/60))
            led = Led(self.getColorLeds(), self.getWindow().getColor(), (x,y), self.getWindow().getWidow_Surface())
            self.getLeds().append(led)
            i+=1
        for led in self.getLeds():
            if self.getWatch().getSecond().getValue() == self.getWatch().getSecond().getMin():
                led.switchOn()
            else:
                if (led.getId() < self.getWatch().getSecond().getValue()):
                    led.switchOn()

        if self.__initDelayPrintTime:
            self.__delayPrintTime = time.time()
        self.print_time()
        pygame.display.flip()
        if self.__initDelayPrintTime:
            self.__delayPrintTime = time.time() - self.__delayPrintTime
            self.__initDelayPrintTime = False
            #print("durée de l'affichage d'un texte : ", self.__delayPrintTime, "s")
        self.__Leds_inverse = self.getLeds().copy()
        self.__Leds_inverse.reverse()
        print("horloge génerée")

    def tick(self):
        global launched
        launched = True
        proc_led = threading.Thread(target = self.process_led)
        proc_led.start()
        while launched:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    launched = False
                    print("durée d'un clignotement : ", 2 * self.__delayBlinking, "s")
                    print("durée d'une raz : ", self.__delayRAZ, "s")
                    print("durée de l'affichage de l'heure : ", self.__delayPrintTime, "s")
                    print("temps de latence totale : ", self.__delayPrintTime + self.__delayRAZ + 2*self.__delayBlinking, "s")

    def process_led(self):
        global launched
        while launched:  # tant qu'on ne quitte pas la fenetre
            nbLedOff = self.getWatch().getSecond().getMax() + 1 - self.getWatch().getSecond().getValue()
            if nbLedOff == self.getWatch().getSecond().getMax() + 1:
                time.sleep(1 - self.__delayRAZ - self.__delayPrintTime)
                self.getWatch().increment()
                print(self.getWatch().getSecond().getValue(), "s")
                self.raz()
                self.print_time()
            else:
                ledPrec = None
                for led in self.getLeds_inverse():
                    if launched:  # tant qu'on ne quitte pas la fenetre
                        if not led.getIsOn():  # si la led en question est deja allumé
                            timeDelay = 1 / nbLedOff
                            if ledPrec != None:
                                time.sleep((timeDelay - 2 * self.__delayBlinking))
                                ledPrec.switchOff()
                            else:
                                time.sleep(timeDelay - self.__delayBlinking)
                            if self.__initDelayBlinking:
                                self.__delayBlinking = time.time()
                            led.switchOn()
                            pygame.display.flip()
                            if self.__initDelayBlinking:
                                self.__delayBlinking = time.time() - self.__delayBlinking
                                self.__initDelayBlinking = False
                                # print("durée d'un clignotement: ",self.__delayBlinking*2, "s")
                            ledPrec = led
                        else:
                            break
                    else:
                        break
                self.getWatch().increment()
                print(self.getWatch().getSecond().getValue(), "s")
        sys.exit(-1)

    def raz(self):
        if self.__initDelayRAZ:
            self.__delayRAZ = time.time()

        for led in self.getLeds():
            if led.getId() > self.getWatch().getSecond().getMin():
                led.switchOff()

        pygame.display.flip()

        if self.__initDelayRAZ:
            self.__delayRAZ = time.time() - self.__delayRAZ
            self.__initDelayRAZ = False


    def getLedByCoord(self,x,y):
        for led in self.getLeds():
            if (led.getCoord()[0] == x)  and (led.getCoord()[1] == y):
                return led
        return None