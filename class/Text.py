import math

import pygame
from Window import Window

class Text:

    def __init__(self, clock):
        self.__police = None
        self.__clock = clock
        self.__x = -((4*math.sqrt(13)+2)*self.__clock.getRadius()-17*self.__clock.getWindow().getCenter()[0])/17
        self.__y =    ((math.sqrt(13)-8)*self.__clock.getRadius() + 17*self.__clock.getWindow().getCenter()[1])/17
        self.__x = int(self.__x)
        self.__y = int(self.__y)
        self.__longueur = (2*self.__clock.getRadius() - 2*(self.__clock.getRadius() - (self.__clock.getWindow().getCenter()[0] - self.__x)))
        self.__largeur = 2*(self.__clock.getWindow().getCenter()[1] - self.__y)


    def convertPixelToPolice(self, pixel):
        cm = pixel/37.7953
        police = cm/0.03527
        return int(police)

    def convertPoliceToPixel(self, police):
        cm = police*0.03527
        pixel = cm*37.7953
        return int(pixel)

    def displayTextCenter(self, taillePixel, text, coord, color):
        self.__police = self.convertPixelToPolice(taillePixel)
        width = coord[0] - len(text)*self.convertPoliceToPixel(self.__police)//6.5
        height = coord[1] - self.convertPoliceToPixel(self.__police)//2
        arial_front = pygame.font.SysFont("arial", self.__police)  # police de caractere
        text = arial_front.render(text, True, color)
        coord = [width, height]
        self.__clock.getWindow().getWidow_Surface().blit(text, coord)  # affichage du text

    def displayTextCenter2(self, taillePixel, text, color):
        self.__police = self.convertPixelToPolice(taillePixel)
        arial_front = pygame.font.SysFont("arial", self.__police)  # police de caractere
        text = arial_front.render(text, True, color)
        x = self.convertPixelToPolice(self.getX())
        y = self.convertPixelToPolice(self.getY())
        self.__clock.getWindow().getWidow_Surface().blit(text, self.__clock.getWindow().getCenter())  # affichage du text
    def displayRectangle(self, taillePixel, color):
        if self.__police == None:
            self.__police = self.convertPixelToPolice(taillePixel)
        longueur = (2*self.__clock.getRadius() - 2*(self.__clock.getRadius() - (self.__clock.getWindow().getCenter()[0] - self.__x)))
        largeur = 2*(self.__clock.getWindow().getCenter()[1] - self.__y)
        pygame.draw.rect(self.__clock.getWindow().getWidow_Surface(), color, pygame.Rect(self.__x, self.__y, longueur, largeur))
        pygame.display.flip()

    def getX(self):
        return self.__x

    def getY(self):
        return self.__y

    def getLongueur(self):
        return self.__longueur
    def getLargeur(self):
        return self.__largeur