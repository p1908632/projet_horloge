from Counter import Counter
import datetime
class Watch:

    def __init__(self):
        time = datetime.datetime.now().strftime("%Y:%m:%d:%H:%M:%S").split(":")
        self.__year = Counter(-float('inf'), int(time[0]),float('inf'))
        self.__month = Counter(1, int(time[1]),12)
        if(1 <= int(time[1]) <= 7) and (int(time[1])%2 == 1) or (8 <= int(time[1]) <= 12) and (int(time[1])%2 == 0):#si c'est janvier, mars, mai, juillet, aout, octobre, ou décembre
            self.__day = Counter(1, int(time[2]), 31)
        else:
            if int(time[1]) == 2: #si c'est février
                self.__day = Counter(1, int(time[2]), 28)
            else:
                self.__day = Counter(1, int(time[2]), 30)
        self.__hour = Counter(0, int(time[3]), 23)
        self.__minute = Counter(0, int(time[4]), 59)
        self.__second = Counter(0, int(time[5]), 59)

    def getYear(self):
        return self.__year

    def getMonth(self):
        return self.__month

    def getDay(self):
        self.__day

    def getHour(self):
        return self.__hour

    def getMinute(self):
        return self.__minute

    def getSecond(self):
        return self.__second

    def printTime(self, boolean):
        string = ""
        if(self.getHour().getValue() < 10):
            string += "0"
        string += str(self.getHour().getValue()) + ":"

        if(self.getMinute().getValue() < 10):
            string += "0"
        string += str(self.getMinute().getValue())

        if boolean == True:
            a = 1
        return string

    def increment(self):
        if self.getSecond().increment():
            if self.getMinute().increment():
                self.getHour().increment()