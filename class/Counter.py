class Counter:

    def __init__(self, min ,value, max):
        self.__min = min
        self.__max = max
        if value < min:
            self.__value = min
        elif value > max:
            self.__value = max
        else:
            self.__value = value

    def getMin(self):
        return self.__min

    def getMax(self):
        return self.__max

    def getValue(self):
        return self.__value

    def increment(self):
        if self.getValue() >= self.getMax():
            self.__value = self.getMin()
            return True
        else:
            self.__value += 1
            return False
