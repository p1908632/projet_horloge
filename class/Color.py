class Color:

    def __init__(self):
        self.BLACK = (0, 0, 0)
        self.BLUE = (0, 0, 255)
        self.GREEN = (0, 255, 0)
        self.RED = (255, 0, 0)
        self.YELLOW = (255, 255, 0)
        self.WHITE = (255, 255, 255)
        self.PURPLE = (166, 16, 173)
        self.PINK = (255, 192, 203)
        self.BROWN = (88, 41, 0)
        self.GREY = (128, 128, 128)

