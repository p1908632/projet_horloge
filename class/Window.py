import pygame
from Color import Color
from tkinter import (Tk)
from Led import Led
class Window:

    def __init__(self,color):
        self.__color = color
        self.__app = Tk()
        self.__width = int(self.__app.winfo_screenwidth()*0.75)
        self.__height = int(self.__app.winfo_screenheight()*0.75)
        self.__center = [self.__width//2, self.__height//2]
        self.__window_surface = None
        pygame.FULLSCREEN  # pleine écran
        pygame.RESIZABLE  # gerer une fenetre redimensionnable
        pygame.NOFRAME  # page sans menu
        pygame.OPENGL
        pygame.HWSURFACE
        pygame.DOUBLEBUF

    def getColor(self):
        return self.__color

    def getWidth(self):
        return self.__width

    def getWidow_Surface(self):
        return self.__window_surface

    def getHeigth(self):
        return self.__height

    def getDimensions(self):
        return (self.getWidth(), self. getHeigth())

    def getCenter(self):
        return self.__center

    def __generateWindowSurface(self):
        return pygame.display.set_mode(self.getDimensions())

    def generate(self):
        pygame.init()  # charger les modules init de python dans le programme
        # print(pygame.display.Info()) #info,vérification
        # print(pygame.get_sdl_version()) # version pygame
        pygame.display.set_caption("Mon programme")  # titre de la page affiché
        self.__window_surface = self.__generateWindowSurface()
        self.__window_surface.fill(self.getColor())
        pygame.display.flip()

    def getCoordCenterText(self, text, police):
        lenght = len(text)//2
        weight = police//2
        return [self.getCenter()[0] - lenght, self.getCenter()[1] + weight]