import pygame

pygame.init()
window_resolution = (640, 480)
blue_color = (89,152,255)
black_color = (0,0,0)
red_color = (255,0,0)

pygame.display.set_caption("Mon programme")  # titre de la page affiché

window_surface = pygame.display.set_mode(window_resolution)
window_surface.fill(blue_color)
pygame.draw.line(window_surface, black_color, [10,10], [100,100])
pygame.display.flip()
launched = True
while launched:
    for event in pygame.event.get():
        if event.type == pygame.KEYUP:
            launched = False
pygame.draw.line(window_surface, red_color, [50,50], [100,100])
pygame.display.flip()
launched = True
while launched:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            launched = False